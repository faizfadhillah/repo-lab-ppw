from django.conf.urls import url
from . import views

#url for app
urlpatterns = [
    url(r'^homepage', views.main, name='main'),
    url(r'^workpage', views.work, name='work'),
    url(r'^trackrecord', views.track, name='track'),
    url(r'^contactme', views.contact, name='contact'),
    url(r'^registeraccount',views.register, name='register'),
    url(r'^addschedule', views.addschedule, name='addschedule'),
    url(r'^saveschedule',views.saveschedule, name='saveschedule'),
    url(r'^scheduleresult',views.scheduleresult, name='scheduleresult'),
    url(r'^emptyschedule',views.scheduledelete, name='scheduledelete'),
    url(r'^', views.main),
]
