from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule

response = {}

# Create your views here.
def main(request):
    context={'home':'active'}
    return render(request, 'Mainpage.html', context)

def work(request):
    context={'workpage':'active'}
    return render(request, 'Workpage.html', context)

def track(request):
    context={'trackrecord':'active'}
    return render(request, 'Trackrecord.html', context)

def contact(request):
    context={'contactme':'active'}
    return render(request, 'Contactme.html', context)

def register(request):
    context={'registeraccount':'active'}
    return render(request, 'Registeraccount.html', context)

def addschedule(request):
    response['saveschedule'] = Schedule_Form
    html = 'Addschedule.html'
    return render(request,html,response)

def saveschedule(request):
    form = Schedule_Form(request.POST or None)
    if(request.method == 'POST'):
        response['day'] = request.POST['day']
        response['date'] = request.POST['date']
        response['event'] = request.POST['event']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        schedule = Schedule(day=response['day'], date=response['date'], event=response['event'],location=response['location'],category=response['category'])
        schedule.save()
        html = 'Addschedule.html'
        return render(request,html,response)
    else:
        return HttpResponseRedirect('/')

def scheduleresult(request):
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    html = 'Scheduleresult.html'
    return render(request, html, response)

def scheduledelete(request):
    schedule = Schedule.objects.all().delete()
    return HttpResponseRedirect('scheduleresult')
