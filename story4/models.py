from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Schedule(models.Model):
    day = models.TextField(max_length=27)
    date = models.DateTimeField()
    event = models.TextField(max_length=100)
    location = models.TextField(max_length=200)
    category = models.TextField(max_length=100)
