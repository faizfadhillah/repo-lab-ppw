from django import forms

class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control'
    }

    day = forms.CharField(label='Day', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput({'class' : 'form-control', 'type' : 'datetime-local'}))
    event = forms.CharField(label='Event', required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
    location = forms.CharField(label='Location', required=False, max_length=200, widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
